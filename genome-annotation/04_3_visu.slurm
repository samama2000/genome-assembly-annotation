#!/bin/bash

#SBATCH --cpus-per-task=1
#SBATCH --mem=40G
#SBATCH --time=1-00:00:00
#SBATCH --job-name=visu1
#SBATCH --mail-user=santiago.marinmartinez@students.unibe.ch
#SBATCH --mail-type=end,fail
#SBATCH --partition=pall

# Set directories
COURSEDIR=/data/users/smarin/assembly-course/genome-annotation
OUTPUTDIR=${COURSEDIR}/04_phylo


# Set parameters
NAME=$1

# Start working directory
cd ${OUTPUTDIR}/${NAME}

# Input files
TE_FILE=color_strip.tsv
SUM_FILE=${COURSEDIR}/EDTA/pilon.fasta.mod.EDTA.TEanno.sum

# Output TSV file
OUTPUT="simple_bar.tsv"

touch $OUTPUT


# Extract TE Order names and corresponding counts
awk -F'#' 'BEGIN {OFS="\t"} {print $1, $2}' "$TE_FILE" | \
while read -r te_order marker; do
    # Include hashtag and marker in the TE Order name for output
    formatted_te_order="${te_order}#${marker}"
    count=$(grep -E "${te_order}" "$SUM_FILE" | grep -E -o '\s[0-9]+\s{11}')
    echo $count
    if [ -n "$count" ]; then
        echo -e "$formatted_te_order\t$count" >> "$OUTPUT"
    fi
done

echo "Output written to $OUTPUT"
